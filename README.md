# TestPrj
[![Quality Gate Status](https://sonar.ad.aruba.it/api/project_badges/measure?project=it.aruba%3Atest-prj-bom&metric=alert_status)](https://sonar.ad.aruba.it/dashboard?id=it.aruba%3Atest-prj-bom)

Prerequisiti:

1. Apache Maven > 3.8
2. Java 17
3. Docker (OPTIONAL)


Per compilare il progetto lanciare il comando maven:

```mvn clean install```

La compilazione creerà il pacchetto spring boot eseguibile sotto la cartella app-boot/target:

**test-prj-app.jar**

L'esecuzione in locale può avvenire lanciando:

```java -jar test-prj-app.jar```

L'applicativo espone la documentazione all'url [Documentation](http://localhost:8080/swagger-ui.html).

Per verificare le dipendenze maven già incluse nel progetto e suoi sotto moduli, lanciare il comando:

```mvn dependency:tree```


Il file Shortcuts.xml contiene una lista di comandi utili per facilitare compilazione/controlli sicurezza/gitflow. 
Per poter usufruire di tutti i comandi common, lanciare prima 
```mvn clean install``` 
e aprire il file con tasto dx -> Add as Ant Build File 



```
project
   |-- pom.xml          # BOM di progetto
   |-- Dockerfile       # File Docker per la creazione dell'immagine
   |-- gitlab-ci.yml    # Pipeline Gitlab
   |-- project-false-positive.xml   # Lista dei falsi positivi per il plugin di OWASP
   |-- shortcuts.xml                # Comandi common per il progetto
   |-- app-boot #Boot Class, Controller and Service layer, implementation of the api, business logic, validation, domain layer for persistence, defining Entities and Repositories
   |   |-- pom.xml
   |   `-- src
   |       |-- main
   |       |   `-- java.it.aruba
   |       |        `-- {BootApplication}.java #Spring Boot Application Class per il progetto
   |       |        `-- controller  # Package dei controller REST
   |       |        `-- service     # Package per la business logic
   |       |        `-- security    # Package per le implementazioni sulla Security Web
   |       |        `-- domain # Package per il model e repository Data   
   |       |        `-- core # Package per Costanti, utilità, classi trasversali ai layer di progetto   
   |           ``- resources 
   |       `-- test
   |           `-- java
```

Per approfondimenti sulla struttura di progetto e configurazioni visionare il documento alla [Sezione Confluence dedicata](https://confluence.aruba.it/display/LGA/Spring-Boot+Based+Project+-+2.+Struttura+di+Progetto)

# GitLab Pipeline

La pipeline di CI/CD è presente nel file .gitlab-ci.yml e fa riferimento alla pipeline standard che crea e publica un'immagine Docker sul Docker Registry di gruppo Nexus.

Per visionare tutti i parametri e il significato delle pipeline visionare il documento presente alla pagina [Confluence Pipeline](https://confluence.aruba.it/display/DOPS/Pipeline+Templates).

# Docker image

La creazione e pubblicazione dell'immagine docker è affidata alla pipeline gitlab che viene triggerata ad ogni push su git.
L'immagine docker è scaricabile in locale lanciando:

> docker pull nexusrepo.aruba.it:{PORTA}/{NOME_IMMAGINE}:{TAG}

Per individuare la porta del proprio docker registry fare riferimento alla pagina [Confluence](https://confluence.aruba.it/display/DC/Nexus+-+Docker).
