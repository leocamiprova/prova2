/* Copyright © Aruba S.p.A. All rights reserved. */
package it.aruba;

import it.aruba.core.bootstrap.AppBootstrap;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestPrjApplication {

    public static void main(String[] args) {
        AppBootstrap.runApplication(TestPrjApplication.class, args);
    }

}
