#FROM nexusrepo.aruba.it:5020/aruba-images/aruba-adoptopenjdk:v1.0.0-jdk-11-hotspot-ubuntu
#FROM openjdk:11-jre-slim
FROM openjdk:17.0.2-jdk-slim

#RUN apt-get -y update
#RUN apt-get -y install telnet
#RUN apt-get -y install curl
#RUN apt-get -y install tcpdump
#RUN apt-get -y install iputils-ping
#RUN apt-get -y install bash
#RUN apt-get -y install grep
#RUN apt-get -y install net-tools

RUN mkdir -p /application

COPY app-boot/target/test-prj-app.jar /application/app.jar

WORKDIR /application

EXPOSE 8080

ENV CONFIG_FILE=classpath:application.properties

CMD ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","app.jar", "--spring.config.additional-location=${CONFIG_FILE}", ">", "/dev/stdout", "2>&1"]



